# Ansible Role: OpenVPN

## Requirements

- Debian 10 (Buster)
- Python-apt

## Role behavior

This role installs and configures OpenVPN. It generates certificates, keys and configuration for server and clients. It permits also to create several users.

### Firewall
This role enables IPv4 forwarding but doesn't edit firewall to add NAT and masquerade rules. In function of your firewall, you must edit your configuration.

### Users
If you want to restrict VPN to some users with authentication, you must to create unix users with openvpn_users_list variable.

Example:
```yaml
openvpn_users_list:
  - name: 'mulder'
    password: 'encryptedpassword'
  - name: 'scully'
    password: 'encryptedpassword'
```

Users's passwords must be encrypted. You can encrypt them thanks to this OpenSSL command :
```shell
openssl passwd -6 mypasswordtoencrypt
```

## Role Variables

Available variables with defaults values are defined in `defaults/main.yml`. You can set custom variables thanks to openvpn_server_custom_configuration and openvpn_client_custom_configuration lists.

### Required variables

- openvpn_remote_host

## Example of configuration
```yaml
# Server configuration
openvpn_remote_host: vpn-example.org
openvpn_server_server: '10.8.0.0 255.255.255.0'

openvpn_server_custom_configuration:
  - 'client-to-client'

# Users generation
openvpn_users_list:
  - name: 'mulder'
    password: 'encryptedpassword'
  - name: 'scully'
    password: 'encryptedpassword'

```

## Credits

Role maintained by Jonas Chopin-Revel on License GNU-GPLv3.

Gitlab : https://framagit.org/jcr/ansible-openvpn

**Share and improve it. It's free !**
